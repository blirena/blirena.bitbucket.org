#+Title: Test suite reduction
#+Author: Irena Blagojević, Nikola Katić @@latex:\\@@ @@html:<br>@@ Gorana Vučić, Stefan Zikić
#+Date: @@latex:\\@@ @@html:<br>@@ May 2019
#+OPTIONS: num:nil toc:nil timestamp:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
#+REVEAL_EXTRA_CSS: ./presentation.css
#+REVEAL_THEME: night
* The problem

** Growing software

   - Increasing number of test cases

   - Large test suites suggest:

     - Long execution time

     - Expensive test suite maintenance

     - Redundant test cases

   - Test suite reduction necessary

** General approach

   CFG-based model

   - $\Phi(t)$ -- set of elements subject to testing

   - Tests $t_1, t_2 \in S$ are *duplicates* iff $\Phi(t_1) = \Phi(t_2)$

   - $\Psi(t, f)$ -- restriction of $\Phi(t)$ to a code unit $f$

   - Test *equivalence* in relation to a code unit

     $$S_1 ≡_f S_2 ~iff~ \bigcup_{t \in S_1} \Psi(t, f) = \bigcup_{t \in S_2} \Psi(t, f)$$

   #+REVEAL: split
   - *Redundant tests* in a test suite

     Tests $t_0, ..., t_n \in S_1$ are redundant in $S \supseteq S_1$ iff

     *$$(\forall f \in \phi(S))~ S ≡_f S_0$$*

     where $S_0 = S \setminus S_1$


* Implemented idea

** General approach

   Coverage-based model

   - $n \times m$ matrix ($n$ tests, $m$ code units)

   - $M_{i,j} = \Psi(t_i, f_j)$

   - *Duplicates* --- corresponding rows are equal element-wise

   - *$S_1 ≡_f S_2$* --- observing column for $f$ only, unions of elements corresponding to $S_1$ and $S_2$ are equal

   - *Goal*: by eliminating tests (i.e. rows), find matrix $M'$ such that

   $$\bigcup_{i,j}M'_{i,j} = \bigcup_{i,j}M_{i,j} $$

** Coverage matrix

   - Stores number of lines covered per file

     - Each row represents a test case

     - Each column represents a file

   - Employing *gcov* (v.9) to obtain coverage information

     - Modified *fastcov* tool

   #+NAME: tab:cov_matrix
   #+ATTR_HTML: :width 50% :height 50%
   |       | $f_0$ | $f_1$ | $f_2$ | $f_3$ | $f_4$ |
   | $t_0$ |    20 |    20 |     0 |     0 |     0 |
   | $t_1$ |    10 |     0 |    10 |     0 |     0 |
   | $t_2$ |     0 |    30 |    30 |     0 |    30 |
   | $t_3$ |     0 |     0 |    10 |    20 |     0 |
   | $t_4$ |     0 |     0 |     0 |    20 |    30 |

** Transforming matrix into SMT2 format
    :PROPERTIES:
    :reveal_background: ./smt2-spacemacs.png
    :END:
   - One clause per file

     - Binary mode

       Among all tests covering a file,

       choose at least  one

     - Non-binary mode

       For each group of tests covering

       x lines in a file, choose at least one test

   - Creating constrains in SMT2 format

   - Employing Z3 to solve the problem

** Implementation
   :PROPERTIES:
   :reveal_background: ./SMTSR_UML_transparent.png
   :END:

   - Python scripts
     - =smtsr.py=
     - =cli.py=
     - =coverage_matrix.py=
     - =reduce.py=
     - =testing_utils.py=

   - Modified =fastcov= tool

   - Easy to extend

* Evaluation

** Qt project using Google tests
   :PROPERTIES:
   :reveal_background: ./gatest_example.png
   :END:

   - Student project
   - 14 source files, 3200 LOC
   - *Initially*: 20 tests, 174 ms
   - *Result*: 15 tests, 6 ms

** LLVM's LLC tool
   :PROPERTIES:
   :reveal_background: ./sparc_llc_example.png
   :END:

   - Open-source project
   - SPARC architecture tests only
   - *Initially*: 85 tests, 18180 ms
   - *Result*: 82 tests, 17820 ms

* Pros and cons

** Pros

   - Preserves coverage

   - Simple architecture

   - Easy to extend

** Cons

   - Not a universal solution

   - Semantics not taken into account

   - Nondeterministic with completely random tests

* Related work

** Test redundancy criteria
    :PROPERTIES:
    :reveal_background: ./related_work.png
    :END:

   - Paths in CFG

   - Coverage

     e.g. statement, branch, condition, function

   - Fault detection

   - Operational abstraction

* Thank you
